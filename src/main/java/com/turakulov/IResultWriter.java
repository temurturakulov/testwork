package com.turakulov;

import java.util.Map;

public interface IResultWriter {
    void write (Map<String,Long> result);
}
